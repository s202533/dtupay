package group16.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/request")
public class RequestController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/info")
    public List info() {
        System.out.println(restTemplate.getForObject("http://dtupay/token/list", List.class));
        return restTemplate.getForObject("http://dtupay/token/list", List.class);
    }
}
